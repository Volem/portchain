const assert = require('assert');
const moment = require('moment');

const port = require('../domain/port');
const vessel = require('../domain/vessel');
const { portCall } = require('../domain/portCall');
const schedule = require('../domain/schedule');

const analysis = require('../service/analysis');

describe('analysis service tests', function () {
	describe('Call duration', function () {
		it('should calculate duration', function () {
			// Arrange 
			const portMock = port('1', 'Test Port 1');
			const portCallMock = portCall(portMock, new Date('2020/01/01 00:00'), new Date('2020/01/01 05:00'));

			// Act
			const duration = analysis.getDuration(portCallMock);

			// Assert
			assert.strictEqual(duration, 18000000, 'port call duration calculation is failed');
		});
		it('should calculate duration as 0 when port call is omitted', function () {
			// Arrange 
			const portMock = port('1', 'Test Port 2');
			const portCallMock = portCall(portMock, new Date('2020/01/01 00:00'), new Date('2020/01/01 05:00'), true);

			// Act
			const duration = analysis.getDuration(portCallMock);

			// Assert
			assert.strictEqual(duration, 0, 'port call duration calculation is failed');
		});
	});

	describe('Analyze Ports', function () {
		// Arrange
		const portMocks = [
			port('1', 'Test Port 1'),
			port('2', 'Test Port 2'),
		];
		const arrival = new Date('2020/01/01 00:00');
		const departure = moment(arrival).add(10, 'hour').toDate();
		const portCallMocks = [
			portCall(portMocks[0], moment(arrival).toDate(), moment(departure).toDate(), false, new Date()),
			portCall(portMocks[0], moment(arrival).add(1, 'day').toDate(), moment(departure).add(30, 'hours').toDate(), false, new Date()),
			portCall(portMocks[1], moment(arrival).toDate(), moment(departure).toDate(), false, new Date()),
			portCall(portMocks[0], moment(arrival).toDate(), moment(departure).toDate(), true, new Date()),
			portCall(portMocks[0], moment(arrival).add(1, 'day').toDate(), moment(departure).add(30, 'hours').toDate(), true, new Date()),
			portCall(portMocks[1], moment(arrival).toDate(), moment(departure).toDate(), true, new Date())
		];
		// 4 portcall from port 0 to vessel 1, 2 omitted
		// 2 portcall from port 1 to vessel 1, 1 omitted
		// 4 portcall from port 0 to vessel 2, 2 omitted
		// 2 portcall from port 1 to vessel 2, 1 omitted
		// 8 portcalls from port 0, 4 portcall from port 1
		const schedulesMock = [
			schedule(vessel(1, 'Vessel 1'), portCallMocks),
			schedule(vessel(2, 'Vessel 2'), portCallMocks)
		];

		// Act
		const statistics = analysis.analyzePorts(schedulesMock);

		it('should calculate basic statistics', function () {

			// Assert
			assert.ok(statistics, 'statistics data is undefined');
			assert.strictEqual(statistics.length, 2, 'statistics data count is wrong');
			assert.ok(statistics[0], 'first port is undefined');
			assert.strictEqual(statistics[0].id, '1', 'first port id is wrong');
			assert.strictEqual(statistics[0].name, 'Test Port 1', 'first port name is wrong');
			assert.strictEqual(statistics[0].callCount, 8, 'First port call count is wrong');
			assert.strictEqual(statistics[0].arrivalCount, 4, 'first port arrival count is wrong');
			assert.strictEqual(statistics[0].callDuration, 187200000, 'first port call duration is wrong');
			assert.ok(statistics[1], 'first port is undefined');
			assert.strictEqual(statistics[1].id, '2', 'second port id is wrong');
			assert.strictEqual(statistics[1].name, 'Test Port 2', 'second port name is wrong');
			assert.strictEqual(statistics[1].callCount, 4, 'second port call count is wrong');
			assert.strictEqual(statistics[1].arrivalCount, 2, 'second port arrival count is wrong');
			assert.strictEqual(statistics[1].callDuration, 72000000, 'second port call duration is wrong');

		});
		it('should calculate call duration percentiles', function () {
			// Act
			const statisticsWithPercentiles = analysis.calculateCallDurationPercentiles([50, 90], statistics);

			// Assert
			assert.ok(statisticsWithPercentiles, 'percentile data is undefined');
			assert.strictEqual(statisticsWithPercentiles.length, 2, 'percentile data count calculation is failed');
			assert.ok(statisticsWithPercentiles[0], 'first port is undefined');
			assert.strictEqual(statisticsWithPercentiles[0].callDurationPercentile, 90, 'first port call duration percentile is wrong');
			assert.ok(statisticsWithPercentiles[1], 'second port is undefined');
			assert.strictEqual(statisticsWithPercentiles[1].callDurationPercentile, 50, 'second port call duration percentile is wrong');
		});
	});
});