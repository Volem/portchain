## Portchain Coding Challenge 

Challange is implemented as a node.js console application. To run the application, execute following commands on the project directory
```
$ npm install

$ npm start
```
This will bring a basic console interface to show the results of the analysis.

To execute unit tests run the npm test command.
```
$ npm test
```

## Development Environment
OS : macOS Catalina  

NodeJS : v14.15.1 - Last LTS during development

IDE : VSCode 1.51 (October 2020)

For debugging purposes, .env file and vscode launch.json configurations are also included at the repository.

Implementation requires only API URL defined at the environment provided at the challange description. NPM start script at package.json already defines the given API URL for simplicity to run. 

## Structure
Generally functional programming approach is used. Implements pure functions and uses immutable objects where possible. Functions are small and composed to reach the end goal.

- *api* : API calls and converts the response into domain objects. Axios http client is used.
- *domain* : Domain objects that are used overall application.
- *service* : Business implementation. Contains analysis functions to accomplish the given tasks. Also prompter is here to provide a UI for console application.
- *tests* : Unit tests. Mocha framework and node.js builtin assert is used.

- Custom eslint rules are included at .eslintrc.json
- jsdoc comments included and also typedefs.js file contains custom types. jsdoc is parsed by vscode so it provides a safer implementation.
