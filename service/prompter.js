const readline = require('readline');

const analysis = require('./analysis');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.on('close', () => process.exit());

// To avoid switch/case operation and reusability of the selected option, a handler object used.
// eslint-disable-next-line jsdoc/require-returns
/**
 * Holds handlers for user selection
 * @param {PortStatistic[]} basicAnalysis - Basic analysis of ports
 * @param {Schedule[]} schedules - Schedules for vessels
 */
const handlers = (basicAnalysis, schedules) => ({
	// Top 5 Ports with the most arrivals
	'1': () => basicAnalysis
		.sort((a, b) => b.arrivalCount - a.arrivalCount)
		.slice(0, 5)
		.map(d => ({
			id: d.id,
			name: d.name,
			arrivalCount: d.arrivalCount,
		})),
	// Top 5 Ports with fewest port calls
	'2': () => basicAnalysis
		.sort((a, b) => a.callCount - b.callCount)
		.slice(0, 5)
		.map(d => ({
			id: d.id,
			name: d.name,
			callCount: d.callCount,
		})),
	// Percentiles of port call durations
	'3': () => analysis.calculateCallDurationPercentiles([5, 20, 50, 75, 90], basicAnalysis)
		.sort((a, b) => b.callDurationPercentile - a.callDurationPercentile)
		.map(d => ({
			id: d.id,
			name: d.name,
			callDurationPercentile: `${d.callDurationPercentile}%`
		})),
	'4': () => analysis.analyzeDelays(schedules, [5, 50, 80]),
	'Q': () => process.exit()
});

/**
 * Console UI prompter
 * @param {PortStatistic[]} basicAnalysis - Analysed data
 * @param {Schedule[]} schedules - Vessel schedules
 */
const prompter = function (basicAnalysis, schedules) {
	const options = `Select an option
1 : Top 5 Ports with the most arrivals
2 : Top 5 Ports with fewest port calls
3 : Percentiles of port call durations
4 : Delay analysis
Q : Quit program\n=> `;

	rl.question(options, function (selection) {
		const selectionHandlers = handlers(basicAnalysis, schedules);
		let data = null;
		if (Object.keys(selectionHandlers).includes(selection)) {
			// Retrive the handler based on selection
			const handler = selectionHandlers[selection];
			data = handler();
		}

		data ? console.table(data) : console.error('Wrong selection');
		prompter(basicAnalysis, schedules);
	});

};

module.exports = {
	prompter,
	handlers
};
