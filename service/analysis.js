const percentile = require('percentile');
const moment = require('moment');
const portStatistic = require('../domain/portStatistic');

/**
 * Calculates the duration of portcall
 * @param {PortCall} portCall - Port call for the vessel
 * @returns {number} - Port call duration
 */
const getDuration = portCall => portCall.isOmitted ? 0 : Math.abs(new Date(portCall.departure) - new Date(portCall.arrival));

/**
 * Calculates basic port statistics based on port calls
 * @param {PortCall[]} portCalls - All port calls for all vessels
 * @returns {PortStatistic[]} Array of port statistic
 */
const getStatistics = function (portCalls) {
	return portCalls.reduce((pre, cur) => {
		const duration = getDuration(cur);
		// processing first port call, the array is empty, so we don't need to check if statistic for port exists or not.
		if (pre.length === 0) {
			pre.push(portStatistic(cur.port, 1, cur.isOmitted ? 0 : 1, duration));
			return pre;
		}
		const stat = pre.find(t => t.id === cur.port.id);
		// port statistic exists, so increment statistic values
		if (stat) {
			stat.callCount++;
			if (!cur.isOmitted) {
				stat.arrivalCount++;
				stat.callDuration += duration;
			}
			return pre;
		}
		// port stat does not exists so initialize first statistics for this port.
		pre.push(portStatistic(cur.port, 1, cur.isOmitted ? 0 : 1, duration));
		return pre;
	}, []);
};
/**
 * Returns the found index of the given duration for percentile values
 * @param {number} duration - Port call duration
 * @param {number[]} percentileStats - Percentile values based on given percentiles  
 * @returns {number} Percentile Index
 */
function getDurationPercentileIndex(duration, percentileStats) {
	for (let i = 0; i < percentileStats.length; i++) {
		if (duration > percentileStats[i]) {
			continue;
		}
		return i;
	}
	return percentileStats.length - 1;
}

/**
 * Calculates call duration percentiles for each port and adds this to statistics object
 * @param {number[]} percentiles - Percentiles that are calculated. ie. [5, 25, 50, 70, 90]
 * @param {PortStatistic[]} statistics - Array of port statistics
 * @returns {PortStatistic[]} - Array of port statistics including duration percentiles
 */
const calculateCallDurationPercentiles = function (percentiles, statistics) {
	const percentileStats = percentile(percentiles, statistics.map(t => t.callDuration));

	return statistics.map(portStat => {
		return {
			...portStat,
			callDurationPercentile: percentiles[getDurationPercentileIndex(portStat.callDuration, percentileStats)]
		};
	});
};

/**
 * Returns an array of all port calls. 
 * @param {Schedule[]} scheduleData - All vessels schedule data
 * @returns {PortCall[]} All port calls for all vessels
 */
const reduceAllPortCalls = function (scheduleData) {
	return scheduleData.reduce((pre, cur) => {
		pre.push(...cur.portCalls);
		return pre;
	}, []);
};

/**
 * Generates the basic statistics of ports from port calls
 * @param {Schedule[]} scheduleData - Schedules of all vessels
 * @returns {PortStatistic[]} Basic statistics of ports
 */
const analyzePorts = function (scheduleData) {
	return getStatistics(reduceAllPortCalls(scheduleData));
};

/**
 * Finds the corresponding delay group for the given arrival day difference 
 * @param {number} tillArrivalDays - Time difference between log entry and arrival date for port call
 * @param {number[]} analyzedDays - Analyzed days for delays. ie. [2, 7, 14] 
 * @returns {number} Value for analyzed day (ie. 2, 7 or 14) or undefined if given difference out of range
 */
const findDayGroupKey = function (tillArrivalDays, analyzedDays) {
	// not analyzed block
	if (tillArrivalDays < analyzedDays[0] || tillArrivalDays > analyzedDays[analyzedDays.length - 1]) {
		return;
	}
	for (let i = 0; i < analyzedDays.length; i++) {
		if (tillArrivalDays > analyzedDays[i]) {
			continue;
		}
		return analyzedDays[i];
	}
};

/**
 *  
 * @param {Schedule} vesselSchedule - Schedule of vessel 
 * @param {*} analyzedDayGroups - Initial object to group day delays for each vessel 
 * @returns {*} Delays with given grouping
 */
const calculateDayDelays = function (vesselSchedule, analyzedDayGroups) {
	const analyzedDays = analyzedDayGroups.map(t => t.day);

	return vesselSchedule.portCalls.reduce((init, currentPortCall) => {
		currentPortCall.logEntries.reduce((pre, cur) => {
			if (currentPortCall.isOmitted) {
				return pre;
			}
			// Forecasted day till arrival
			const tillArrivalDays = Math.abs(moment(cur.createdDate).diff(moment(currentPortCall.arrival), 'days'));
			const groupKey = findDayGroupKey(tillArrivalDays, analyzedDays);
			if (cur.arrival && groupKey) {
				// Forecast delay in hours
				const delay = Math.abs(moment(currentPortCall.arrival).diff(cur.arrival, 'hours'));
				const dayGroup = pre.find(t => t.day === groupKey);
				if (dayGroup) {
					dayGroup.delays.push(delay);
				} else {
					pre.push({ day: groupKey, delays: [delay] });
				}
			}
			return pre;
		}, init);
		return init;
	}, analyzedDayGroups);
};

const setDelayPercentiles = (vessel, delays, fromArrivalDay, requestedPercentiles) => {
	const percentiles = percentile(requestedPercentiles, delays);
	percentiles.forEach((percentile, index) => {
		const propertyName = `${requestedPercentiles[index]}th(${fromArrivalDay})`;
		vessel[propertyName] = percentile;
	});
	return vessel;
};

/**
 * Calculates delay percentiles for 2, 7 and 14 day from arrival
 * @param {Schedule[]} scheduleData - schedules of all vessels
 * @param {Array<number>} requestedPercentiles - requested percentile values
 * @returns {*} Vessel's delay percentiles
 */
const analyzeDelays = function (scheduleData, requestedPercentiles) {
	const calculatedDelays = scheduleData.map(s => ({
		vessel: s.vessel,
		nDayDelays: calculateDayDelays(s, [{ day: 2, delays: [] }, { day: 7, delays: [] }, { day: 14, delays: [] }])
	}));

	const vesselWithDelays = [];
	calculatedDelays.forEach(delayData => {
		const vessel = { ...delayData.vessel };
		// Calculate delay percentiles and store it as a property of vessel.
		delayData.nDayDelays.reduce((pre, cur) => {
			const vesselWithDelayData = pre.find(t => t.imo == vessel.imo);
			if (vesselWithDelayData) {
				setDelayPercentiles(vesselWithDelayData, cur.delays, cur.day, requestedPercentiles);
				return pre;
			}
			setDelayPercentiles(vessel, cur.delays, cur.day, requestedPercentiles);
			pre.push(vessel);
			return pre;
		}, []);
		vesselWithDelays.push(vessel);
	});

	return vesselWithDelays;
};

module.exports = {
	getDuration,
	analyzePorts,
	calculateCallDurationPercentiles,
	analyzeDelays
};