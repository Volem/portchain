const api = require('./api');
const analysis = require('./service/analysis');
const { prompter, handlers } = require('./service/prompter');

setImmediate(async () => {
	try {
		console.log('Fetching and processing data...');

		const vessels = await api.vessels.getVessels();

		// We can make all the requests in parallel, no deed to wait one by one to receive all the schedules of vessels.
		// To accomplish this, I created a promise array and await them all to finalize.
		const schedulePromises = vessels.map(vessel => api.schedule.getSchedule(vessel.imo));
		const schedules = await Promise.all(schedulePromises);

		const basicAnalysis = analysis.analyzePorts(schedules);
		if (process.env.DEBUG) {
			Object.values(handlers(basicAnalysis, schedules)).forEach(handler => console.table(handler()));
			return;
		} 
		prompter(basicAnalysis, schedules);
	} catch (ex) {
		console.log(ex);
	}
});