/**
 * Generates port statistic object
 * @param {Port} port - Port object
 * @param {number} callCount - Port call count
 * @param {number} arrivalCount - Arrival count
 * @param {number} callDuration - Port call duration (departure - arrival)
 * @returns {PortStatistic} Port statistic object
 */
module.exports = function (port, callCount = 0, arrivalCount = 0, callDuration = 0) {
	return {
		...port,
		callCount,
		arrivalCount,
		callDuration
	};
};
