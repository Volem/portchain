/**
 * Generates port object
 * @param {string} id - Port id
 * @param {string} name - Port name
 * @returns {Port} Port Object
 */
module.exports = function (id, name) {
	return {
		id,
		name
	};
};
