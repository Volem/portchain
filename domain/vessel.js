/**
 * Generates Vessel Object 
 * @param {number} imo - IMO of Vessel
 * @param {string} name - Name of Vessel
 * @returns {Vessel} Vessel Object
 */
module.exports = function (imo, name) {
	return {
		imo,
		name
	};
};