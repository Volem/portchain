/**
 * Generates Port Call Object
 * @param {Port} port - Port Object
 * @param {Date} arrival - Arrival date
 * @param {Date} departure - Departure date
 * @param {boolean} isOmitted - Is port call omitted or not
 * @param {Date} createdDate - Port calls create date
 * @param {PortCallLogEntry[]} logEntries - Logs of port call
 * @returns {PortCall} PortCall Object
 */
function portCall(port, arrival, departure, isOmitted, createdDate, logEntries) {
	return {
		port,
		arrival,
		departure,
		isOmitted,
		createdDate,
		logEntries
	};
}
/**
 * Generates Port Call Log Entry Object
 * @param {Date} arrival - Arrival date
 * @param {Date} departure - Departure date
 * @param {boolean} isOmitted - Is port call omitted or not
 * @param {Date} createdDate - Port calls create date
 * @param {string} updatedField - Updated field of the port call
 * @returns {PortCallLogEntry} PortCallLogEntry Object
 */
function portCallLogEntry(arrival, departure, isOmitted, createdDate, updatedField) {
	return {
		arrival,
		departure,
		isOmitted,
		createdDate,
		updatedField
	};
}

module.exports = {
	portCall,
	portCallLogEntry
};
