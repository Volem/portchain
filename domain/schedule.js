/**
 * Generates Schedule Object of the vessel 
 * @param {Vessel} vessel - Vessel of schedule
 * @param {PortCall[]} portCalls - Port calls of the vessel
 * @returns {Schedule} Schedule Object
 */
module.exports = function (vessel, portCalls) {
	return {
		vessel,
		portCalls
	};
};