const axios = require('axios').default;

const vessel = require('../domain/vessel');
const port = require('../domain/port');
const { portCall, portCallLogEntry } = require('../domain/portCall');
const schedule = require('../domain/schedule');

const { scheduleEndpoint } = require('../config');

/**
 * Generates port call log entry from api response
 * @param {*} l - Port call log entry api response data
 * @returns {PortCallLogEntry} PortCallLogEntry object
 */
const mapPortCallLogEntry = l => new portCallLogEntry(
	l.arrival,
	l.departure,
	l.isOmitted,
	l.createdDate,
	l.updatedField);

/**
 * Generates port call from API response
 * @param {p} p - Port call API response data
 * @returns {PortCall} PortCall Object
 */
const mapPortCall = p => new portCall(
	new port(p.port.id, p.port.name),
	p.arrival,
	p.departure,
	p.isOmitted,
	p.createdDate,
	p.logEntries.map(mapPortCallLogEntry).sort((a, b) => new Date(a.createdDate) - new Date(b.createdDate))
);

/**
 * Generates parsed data model for vessel schedule, which we refer this as Schedule in the system
 * @param {*} s - API response for a vessel port calls
 * @returns {Schedule} Schedule Object
 */
const mapSchedule = s => new schedule(
	new vessel(
		s.vessel.imo,
		s.vessel.name
	),
	s.portCalls.map(mapPortCall)
);

/**
 * Requests the schedule from API by vessel IMO
 * @param {number} vesselImo - IMO of the vessel
 * @returns {Promise<Schedule>} Schedule Object Promise
 */
const getSchedule = async function (vesselImo) {
	const scheduleResponse = await axios.get(`${scheduleEndpoint}/${vesselImo}`);
	return mapSchedule(scheduleResponse.data);
};

module.exports = {
	getSchedule
};
