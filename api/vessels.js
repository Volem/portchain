const axios = require('axios').default;

const vessel = require('../domain/vessel');
const { vesselsEndpoint } = require('../config');

/**
 * Gets the Vessels from API
 * @returns {Promise<Vessel[]>} Array of Vessel Object Promise
 */
const getVessels = async function () {
	const vesselsResponse = await axios.get(vesselsEndpoint);
	return vesselsResponse.data.map(v => new vessel(v.imo, v.name));
};

module.exports = {
	getVessels
};