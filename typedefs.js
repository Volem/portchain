/**
 * @typedef Port Port Object
 * @property {string} id - Id of Port
 * @property {string} name - Name of Port
 * 
 * @typedef PortStatistic Statistics of Ports based on port calls
 * @property {Port} port - Port object
 * @property {number} callCount - Port call count
 * @property {number} arrivalCount - Arrival count
 * @property {number} callDuration - Port call duration (departure - arrival)
 * @property {number} [callDurationPercentile] - Port call duration percentile
 * 
 * @typedef PortCall PortCall Object
 * @property {Port} port - Port Object
 * @property {Date} arrival - Arrival date
 * @property {Date} departure - Departure date
 * @property {boolean} isOmitted - Is port call omitted or not
 * @property {Date} createdDate - Port calls create date
 * @property {PortCallLogEntry[]} logEntries - Logs of port call
 * 
 * @typedef PortCallLogEntry PortCallLogEntry Object
 * @property {Date} arrival - Arrival date
 * @property {Date} departure - Departure date
 * @property {boolean} isOmitted - Is port call omitted or not
 * @property {Date} createdDate - Port calls create date
 * @property {string} updatedField - Updated field of the port call
 * 
 * @typedef Vessel Vessel Object
 * @property {number} imo - IMO of Vessel
 * @property {string} name - Name of Vessel
 * 
 * @typedef Schedule Schedule Object
 * @property {Vessel} vessel - Vessel of schedule
 * @property {PortCall[]} portCalls - Port calls of the vessel
*/