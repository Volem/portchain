const apiURL = process.env.apiURL;

module.exports = {
	apiURL,
	vesselsEndpoint : `${apiURL}/vessels`,
	scheduleEndpoint: `${apiURL}/schedule`
};